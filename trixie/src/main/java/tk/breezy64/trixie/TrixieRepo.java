package tk.breezy64.trixie;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class TrixieRepo {

	private final List<TrixieLoader> loaders;
	
	public TrixieRepo() {
		this.loaders = new ArrayList<>();
	}
	
	public void loadJar(File jarFile) throws IOException {
		try (InputStream stream = new FileInputStream(jarFile)) {
			TrixieLoader ldr = new TrixieLoader();
			ldr.loadJar(stream);
			loaders.add(ldr);
		}
	}
	
	public List<Class<?>> getAllClasses() {
		return loaders.stream()
				.map(x -> x.getLoadedClasses())
				.collect(ArrayList::new, ArrayList::addAll, ArrayList::addAll);
	}
	
	@SuppressWarnings("unchecked")
	public <T> List<Class<T>> getImplementations(Class<T> baseClass) {
		return getAllClasses()
				.stream()
				.filter(baseClass::isAssignableFrom)
				.map(x -> (Class<T>)x)
				.collect(Collectors.toList());
	}
}
