package tk.breezy64.trixie;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;

import org.apache.commons.io.IOUtils;

public class TrixieLoader extends ClassLoader {

	private Map<String, Class<?>> classCache;
	
	public TrixieLoader() {
		this.classCache = new HashMap<>();
	}
	
	public void loadJar(InputStream jarStream) throws IOException {
		try (JarInputStream jar = new JarInputStream(jarStream)) {
			List<Class<?>> classes = new ArrayList<>();
            JarEntry je;
			while ((je = jar.getNextJarEntry()) != null) {
				String className = je.getName().replace("/", ".").substring(0, Math.max(0, je.getName().length() - 6));
				if (!(je.isDirectory() || classCache.containsKey(className) || !je.getName().endsWith(".class"))) {
					ByteArrayOutputStream tmp = new ByteArrayOutputStream();
					IOUtils.copy(jar, tmp);
					byte[] data = tmp.toByteArray();
					Class<?> cl = defineClass(className, data, 0, data.length);
					classCache.put(className, cl);
					classes.add(cl);
				}
				jar.closeEntry();
			}
			
			for (Class<?> cl : classes) {
				resolveClass(cl);
			}
		}
	}
	
	@Override
	public Class<?> loadClass(String name) throws ClassNotFoundException {
		try {
			Class<?> sysClass = findSystemClass(name);
			if (sysClass != null) {
				return sysClass;
			}
		}
		catch (ClassNotFoundException e) {
			
		}
		
		Class<?> result = classCache.get(name);
		
		if (result != null) {
			return result;
		}
		
		throw new ClassNotFoundException("Could not load class " + name);
	}
	
	public List<Class<?>> getLoadedClasses() {
		return new ArrayList<>(classCache.values());
	}
	
}
