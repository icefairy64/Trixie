package tk.breezy64.trixie;

import java.lang.reflect.Constructor;
import java.util.Arrays;

public class TrixieHat {
	
	public static <T> T create(Class<T> cl) {
		try {
			return cl.newInstance();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	public static <T> T create(Class<T> cl, Object... params) {
		try {
			Class<?>[] paramTypes = Arrays.stream(params)
					.map(x -> x.getClass())
					.toArray(Class<?>[]::new);
			Constructor<T> ctor = cl.getDeclaredConstructor(paramTypes);
			return ctor.newInstance(params);
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
}
